package com.production.namlun.chemistry.Model;

/**
 * Created by Khac Linh on 14/12/2016.
 */

public class Video {
    private int id_Video;
    private String link_Video;
    private int id_Lesson;

    public Video(int id_Video, String link_Video, int id_Lesson) {
        this.id_Video = id_Video;
        this.link_Video = link_Video;
        this.id_Lesson = id_Lesson;
    }

    public int getId_Video() {
        return id_Video;
    }

    public void setId_Video(int id_Video) {
        this.id_Video = id_Video;
    }

    public String getLink_Video() {
        return link_Video;
    }

    public void setLink_Video(String link_Video) {
        this.link_Video = link_Video;
    }

    public int getId_Lesson() {
        return id_Lesson;
    }

    public void setId_Lesson(int id_Lesson) {
        this.id_Lesson = id_Lesson;
    }
}

package com.production.namlun.chemistry.Model;

/**
 * Created by Khac Linh on 14/12/2016.
 */

public class Knowlege {
    private int id_Knowlege;
    private  int id_Lesson;
    private String name_Knowlege;
    private String content_Knowlege;

    public Knowlege(int id_Knowlege, int id_Lesson, String name_Knowlege, String content_Knowlege) {
        this.id_Knowlege = id_Knowlege;
        this.id_Lesson = id_Lesson;
        this.name_Knowlege = name_Knowlege;
        this.content_Knowlege = content_Knowlege;
    }

    public int getId_Knowlege() {
        return id_Knowlege;
    }

    public void setId_Knowlege(int id_Knowlege) {
        this.id_Knowlege = id_Knowlege;
    }

    public int getId_Lesson() {
        return id_Lesson;
    }

    public void setId_Lesson(int id_Lesson) {
        this.id_Lesson = id_Lesson;
    }

    public String getName_Knowlege() {
        return name_Knowlege;
    }

    public void setName_Knowlege(String name_Knowlege) {
        this.name_Knowlege = name_Knowlege;
    }

    public String getContent_Knowlege() {
        return content_Knowlege;
    }

    public void setContent_Knowlege(String content_Knowlege) {
        this.content_Knowlege = content_Knowlege;
    }
}

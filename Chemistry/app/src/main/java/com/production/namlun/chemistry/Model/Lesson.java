package com.production.namlun.chemistry.Model;

/**
 * Created by Khac Linh on 14/12/2016.
 */

public class Lesson {
    private int id_Lesson;
    private String name_Lesson;
    private int level_Lesson;
    private int score;


    public Lesson(int id_Lesson, String name_Lesson, int level_Lesson, int score) {
        this.level_Lesson = level_Lesson;
        this.score = score;
        this.name_Lesson = name_Lesson;
        this.id_Lesson = id_Lesson;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getId_Lesson() {
        return id_Lesson;
    }

    public void setId_Lesson(int id_Lesson) {
        this.id_Lesson = id_Lesson;
    }

    public String getName_Lesson() {
        return name_Lesson;
    }

    public void setName_Lesson(String name_Lesson) {
        this.name_Lesson = name_Lesson;
    }

    public int getLevel_Lesson() {
        return level_Lesson;
    }

    public void setLevel_Lesson(int level_Lesson) {
        this.level_Lesson = level_Lesson;
    }
}

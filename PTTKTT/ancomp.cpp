#include <iostream>
#define MAX 1004
#define ii pair<int,int>
#include<stdlib.h>
#include<algorithm>


using namespace std;
int n;
int v[MAX], comp[MAX]; //v là chỉ số màu được đánh trong cây khung,
int adj[MAX][MAX]; // ma trân input
bool dao[MAX];
pair<ii, ii> P[MAX];
bool dfs(int idx,int itr)
{
    comp[idx]=itr;
    if(v[idx]==1)
        P[itr].second.second++; // đếm số điểm khác màu với gốc trong cây khung
    else
        P[itr].second.first++;  // đếm số điểm cùng màu với gốc trong cây khung
    int i;
    for(i=0; i<n; i++)
    {
        if(adj[idx][i]==1)
        {
            if(v[i]==v[idx]) // kiểm tra xem có cùng màu hay ko
                return true;
            else if(v[i]==-1)
            {
                v[i]=(v[idx]+1)%2;
                if(dfs(i,itr))
                    return true;
            }
        }
    }
    return false;
}
int main()
{
    int test;
    cin>>test;
    int x,y;
    while(test--)
    {
        cin>>n;
        for(int i=0; i<n; i++)
        {
            v[i]=-1;
            P[i].first.first=0;
            P[i].first.second=0;
            P[i].second.first=0;
            P[i].second.second=0;
        }
        for(int i = 0; i<n; i++)
        {
            for(int j =0; j<n; j++)
            {
                cin>>adj[i][j];
            }
        }
        bool flag = false;
        // kiểm tra đường chéo
        for(int i =0; i<n; i++)
        {
            if(adj[i][i] == 1)
            {
                flag = true;
                break;
            }
        }
        if(flag)
        {
            cout<<-1<<endl;
            continue;
        }

        int itr=-1;
        for(int i =0;i<n;i++)
        {
            if(v[i]==-1)
            {
                itr++;
                v[i]=0;
                P[itr].first.second=itr;
                if(dfs(i,itr))
                {
                    flag=true;
                    break;
                }
                P[itr].first.first=abs(P[itr].second.first-P[itr].second.second); // giá trị số điểm trong cây khung khác nhau
            }
        }
        if(flag)
        {
            cout<<-1<<endl;
            continue;
        }
        sort(P,P+itr+1); // sort theo giá trị đầu tiên từ bé đến lớn
        reverse(P,P+itr+1); //Đảo chỗ đầu tiên đên itr +1
        int zero=P[0].second.first,one=P[0].second.second; // số cạnh màu 0 và số cạnh màu 1
        dao[P[0].first.second]=false;
        // xét từ cây khung duyệt cuối cùng và có độ lệch cao nhất

        for(int i=1;i<itr+1;i++)
        {
            x=P[i].second.first;
            y=P[i].second.second;
            if(abs((zero+x)-(one+y))<=abs((zero+y)-(one+x))) //
            {
                zero+=x;
                one+=y;
                dao[P[i].first.second]=false;
            }
            else
            {
                zero+=y;
                one+=x;
                dao[P[i].first.second]=true; // đánh dấu cây khung cần thay đổi
            }
        }
           for(int i=0;i<n;i++) // xét tại từng gốc cây khung xem tại gốc cây khung nào cần đổi màu
        {
            if(dao[comp[i]]) // nếu vị trí cây khung cần đổi thì đổi màu
                v[i]=(v[i]+1)%2;
        }
        for(int i=0;i<n;i++){
            cout<<v[i]<<" ";
        }
        cout<<endl;
    }
    return 0;
}

package com.production.namlun.hoahoc.model;

/**
 * Created by Khac Linh on 14/12/2016.
 */

public class Quetion {
    private int id_Quetion;
    private String content_Quetion;
    private int level_Quetion;

    public int getId_Quetion() {
        return id_Quetion;
    }

    public void setId_Quetion(int id_Quetion) {
        this.id_Quetion = id_Quetion;
    }

    public String getContent_Quetion() {
        return content_Quetion;
    }

    public void setContent_Quetion(String content_Quetion) {
        this.content_Quetion = content_Quetion;
    }

    public int getLevel_Quetion() {
        return level_Quetion;
    }

    public void setLevel_Quetion(int level_Quetion) {
        this.level_Quetion = level_Quetion;
    }

    public int getNumber_Anwser() {
        return number_Anwser;
    }

    public void setNumber_Anwser(int number_Anwser) {
        this.number_Anwser = number_Anwser;
    }

    public String getAnwser_a() {
        return anwser_a;
    }

    public void setAnwser_a(String anwser_a) {
        this.anwser_a = anwser_a;
    }

    public String getAnwser_b() {
        return anwser_b;
    }

    public void setAnwser_b(String anwser_b) {
        this.anwser_b = anwser_b;
    }

    public String getAnwser_c() {
        return anwser_c;
    }

    public void setAnwser_c(String anwser_c) {
        this.anwser_c = anwser_c;
    }

    public String getAnwser_d() {
        return anwser_d;
    }

    public void setAnwser_d(String anwser_d) {
        this.anwser_d = anwser_d;
    }

    public int getAnwser_correct() {
        return anwser_correct;
    }

    public void setAnwser_correct(int anwser_correct) {
        this.anwser_correct = anwser_correct;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    private int number_Anwser;
    private String anwser_a;
    private String anwser_b;
    private  String anwser_c;
    private  String anwser_d;
    private int anwser_correct;

    public Quetion(int id_Quetion, String content_Quetion, int level_Quetion, int number_Anwser, String anwser_a, String anwser_b, String anwser_c, String anwser_d, int anwser_correct, String explain) {
        this.id_Quetion = id_Quetion;
        this.content_Quetion = content_Quetion;
        this.level_Quetion = level_Quetion;
        this.number_Anwser = number_Anwser;
        this.anwser_a = anwser_a;
        this.anwser_b = anwser_b;
        this.anwser_c = anwser_c;
        this.anwser_d = anwser_d;
        this.anwser_correct = anwser_correct;
        this.explain = explain;
    }

    private String explain;
}

package com.production.namlun.hoahoc.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.production.namlun.hoahoc.R;
import com.production.namlun.hoahoc.adapter.LessonAdapter;
import com.production.namlun.hoahoc.model.Lesson;

import java.util.ArrayList;


/**
 * Created by Khac Linh on 16/12/2016.
 */

public class LessonActivity extends Fragment {
    public LessonActivity(){
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lesson_main,container,false);
        TextView tv_nameLesson = (TextView)view.findViewById(R.id.detail);
        tv_nameLesson.setText("Danh sách các bài học");
        ListView listView = (ListView) view.findViewById(R.id.list_lesson);

        listView.setAdapter(new LessonAdapter(getContext(),CreatData()));
        return view;
    }


    public ArrayList<Lesson> CreatData(){
        ArrayList<Lesson> list = new ArrayList<>();
        Lesson lesson = new Lesson(1,"Chương 1",10,2);
        for(int i = 0; i<100;i++){
            list.add(lesson);
        }
        return  list;
    }


}

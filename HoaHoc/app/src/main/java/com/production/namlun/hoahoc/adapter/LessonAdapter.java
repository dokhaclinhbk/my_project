package com.production.namlun.hoahoc.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.production.namlun.hoahoc.R;
import com.production.namlun.hoahoc.model.Lesson;

import java.util.ArrayList;

/**
 * Created by Khac Linh on 17/12/2016.
 */

public class LessonAdapter extends BaseAdapter {
    private  Context context;
    private LayoutInflater layoutInflater;
    ArrayList<Lesson> lessons;
    public LessonAdapter(Context context, ArrayList<Lesson> lessons) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.lessons = lessons;
    }

    @Override
    public int getCount() {
        return lessons.size();
    }

    @Override
    public Object getItem(int position) {
        return lessons.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if(convertView==null){
            convertView = layoutInflater.inflate(R.layout.lesson_layout, null);
            holder = new ViewHolder();
            holder.imageView = (ImageView)convertView.findViewById(R.id.image_lesson);
            holder.tvNameLesson = (TextView)convertView.findViewById(R.id.tvname_lession);
            holder.tvScore = (TextView)convertView.findViewById(R.id.tvscore);
            holder.rltScore = (RelativeLayout)convertView.findViewById(R.id.rlt_score);
            holder.imageScore = (ImageView)convertView.findViewById(R.id.image_score);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        Lesson lesson =lessons.get(position);
        holder.tvNameLesson.setText(lesson.getName_Lesson());
        Log.e("Khac Linh", lesson.getName_Lesson());
        if(lesson.getScore()!=0){
            holder.tvScore.setText(""+lesson.getScore());
        }else{

        }
        return convertView;
    }
    static  class ViewHolder{
        ImageView imageView;
        TextView tvScoreMax;
        ImageView image_book;
        TextView tv1;
        TextView tvNameLesson;
        TextView tvScore;
        ImageView imageScore;
        RelativeLayout rltScore;
    }

}
